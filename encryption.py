import base64
import binascii
import os
import random

from Crypto.Cipher import PKCS1_OAEP, AES
from Crypto.PublicKey import RSA
from Crypto.Util.number import *


"""
Encryptor Class:
    Available Methods:
        __init__(caesarShift) => Initializes an Encryptor class instance with provided caesarShift (default=1)
        base64it(data) => returns any data converted into base64 string
        applyCaesarShift(data) => Applies Caesar shift to each byte of data provided.
        encryptData(data) => Encrypts the data provided with AES-128-CBC algorithm, 
                             and returns hex encoded encrypted data.
        getEncryptedKey() => Encrypts the key used to encrypt the data with RSA-2048 private key and returns hex-encoded
                             encrypted key.
        storePrivateKey(path, passphrase) => Stores the RSA-2048 private key at specified path, protected with the 
                                             supplied passphrase, PEM Encoded, PKCSv5
        encryptFile(path, output) => Encrypts the file and stores the encrypted file to specified output. Default path
                                       is in the same directory as original filename with prefix "Encrypted_".
"""


def base64it(data):
    return base64.b64encode(data)

def getPadding(data):
    return b'\x00'*(16 - (len(data)%16))


class Encryptor:
    privateKey = None
    publicKey = None
    encryptionKey = None
    caesarShift = None
    dataCipher = None
    keyCipher = None

    def __init__(self, caesarShift: int = 1):
        self.privateKey = RSA.generate(2048, randfunc=None)
        self.publicKey = self.privateKey.publickey()
        self.caesarShift = caesarShift%26
        self.keyCipher = PKCS1_OAEP.new(self.privateKey)

    def applyCaesarShift(self, data):
        newData = b''
        for char in data:
            newData += long_to_bytes(char + self.caesarShift)
        return newData

    def encryptData(self, data):
        if len(data)%16 != 0:
            padding = getPadding(data)
        else:
            padding = b''
        return self.dataCipher.encrypt(data+padding), padding

    def getEncryptedKey(self):
        return binascii.hexlify(self.keyCipher.encrypt(self.encryptionKey))

    def storePrivateKey(self, path, passphrase):
        with open(os.path.abspath(path), 'wb') as f:
            f.write(self.privateKey.export_key(format="PEM", passphrase=passphrase, pkcs=5))
            f.close()

    def encryptFile(self, path, output = ''):
        self.encryptionKey = long_to_bytes(random.getrandbits(128))
        self.dataCipher = AES.new(key=self.encryptionKey, mode=AES.MODE_CBC)
        absoluteFilePath = os.path.abspath(path)
        absoluteDir = os.path.dirname(absoluteFilePath)
        if output != '':
            absoluteOutput = os.path.abspath(output)
        else:
            absoluteOutput = os.path.join(absoluteDir,"Encrypted_"+os.path.basename(absoluteFilePath))
        if absoluteOutput == absoluteFilePath:
            raise Exception("Output file path cannot be same as source file")
        try:
            with open(os.path.abspath(path), 'rb') as f:
                originalData = f.read()
                base64Data = base64it(originalData)
                caesarData = self.applyCaesarShift(base64Data)
                encryptedData, padding = self.encryptData(caesarData)
                data = self.getEncryptedKey() + b'.datanoid.' + \
                       binascii.hexlify(self.keyCipher.encrypt(padding)) + \
                       b'.' + binascii.hexlify(self.keyCipher.encrypt(self.dataCipher.iv))+ \
                       b'\n' + encryptedData
                outfile = open(absoluteOutput, 'wb')
                outfile.write(data)
                outfile.close()
                f.close()
        except Exception as e:
            print("Exception has occurred: {}".format(e.with_traceback(None)))
            exit(69)
