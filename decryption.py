import base64
import binascii
import os

from Crypto.Cipher import PKCS1_OAEP, AES
from Crypto.PublicKey import RSA
from Crypto.Util.number import *

"""
Decrypter Class:
    Available Methods:
        __init__(keyPath, password, caesarShift) => Initializes an Decrypter class instance with provided private key
                                                    path, password for the private key, and caesarShift 
        decodeBase64(data) => returns any base64 data converted into original data.
        readHeader(header) => Reads the header of file and checks whether the file is an Datanoid encrypted file or not.
        decryptHeader(header) => Decrypts the header of a datanoid encrypted file and returns the encryption Key,
                                 padding, and IVs which were used to encrypt the file.
        decryptCaesarShift(data) => Reverses the applied caesar shift. 
        decryptFile(path, output) => Decrypts the file and stores the decrypted file to specified output. Default path
                                     is in the same directory as original filename with prefix "Decrypted_".
"""

def decodeBase64(data):
    return base64.b64decode(data)


def readHeader(header):
    if len(header) != 4:
        raise Exception("Unsupported file")
    if header[1] != b'datanoid':
        raise Exception("Unsupported file")
    return True


class Decrypter:
    privateKey = None
    keyCipher = None
    dataCipher = None
    caesarShift = None
    def __init__(self, keyPath, passphrase, caesarShift):
        try:
            with open(os.path.abspath(keyPath), 'rb') as f:
                self.privateKey = RSA.import_key(f.read(), passphrase)
                f.close()
            self.caesarShift = caesarShift
            self.keyCipher = PKCS1_OAEP.new(self.privateKey)
        except Exception as e:
            print("Exception has occurred: {}".format(e))
            exit(69)

    def decryptHeader(self, header):
        return self.keyCipher.decrypt(binascii.unhexlify(header[0])), \
               self.keyCipher.decrypt(binascii.unhexlify(header[2])), \
               self.keyCipher.decrypt(binascii.unhexlify(header[3].strip()))

    def decryptCaesarShift(self, data):
        newData = b''
        for char in data:
            newData += long_to_bytes(char - self.caesarShift)
        return newData

    def decryptFile(self, inpath, outpath):
        inputFilePath = os.path.abspath(inpath)
        absoluteDir = os.path.dirname(inputFilePath)
        if outpath != '':
            outputFilePath = os.path.abspath(outpath)
        else:
            outputFilePath = os.path.join(absoluteDir,"Decrypted_"+os.path.basename(inputFilePath))
        if outputFilePath == inputFilePath:
            raise Exception("Output file path cannot be same as source file")
        try:
            with open(inputFilePath, 'rb') as inputFile:
                header = inputFile.readline().split(b'.')
                if readHeader(header):
                    decryptionKey, padding, iv = self.decryptHeader(header)
                    self.dataCipher = AES.new(decryptionKey, AES.MODE_CBC, iv=iv)
                    decryptedData = self.dataCipher.decrypt(inputFile.read())
                    decryptedData = decryptedData.rstrip(padding)
                    decryptedData = self.decryptCaesarShift(decryptedData)
                    decryptedData = decodeBase64(decryptedData)
                inputFile.close()
            with open(outputFilePath, 'wb') as outputFile:
                outputFile.write(decryptedData)
                outputFile.close()
        except Exception as e:
            print("An error has occurred: {}".format(e.with_traceback(None)))
            exit(69)