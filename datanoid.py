#!/usr/bin/env python3
from encryption import *
from decryption import *
from getpass import getpass
if __name__ == "__main__":
    print("""\n
             ##########################################\n
             #                                        #\n
             #         Welcome to Datanoid 2.0        #\n
             #                                        #\n
             ##########################################\n
    
             (1) Encrypt File(s)
             (2) Decrypt File(s)
             (0) EXIT
             What to do?(1,2 or 0):""")
    choice = int(input())
    filesDone = 0

    if choice == 1:
        shift = int(input("Enter a secret number (!! Remember this. Write it down or you might lose access "
                          "to your files !!): "))
        encryptor = Encryptor(shift)
        noOfFiles = int(input("Enter the number of files you want to encrypt: "))
        while filesDone < noOfFiles:
            file = input("Enter the path of file to encrypt: ")
            output = input("Enter the path of output file. (Default = \"Encrypted_\"+Original File Name): ")
            encryptor.encryptFile(file, output)
            filesDone += 1
            print("Encrypted {} successfully. {} files remaining.".format(file, noOfFiles-filesDone))
        print("All files have been successfully encrypted.")
        keyPath = input("Enter the path to store the private key: ")
        passphrase = getpass("Enter the password to store the key: ")
        encryptor.storePrivateKey(keyPath, passphrase)
    elif choice == 2:
        shift = int(input("Enter the secret number used to encrypt the files: "))
        keyPath = input("Enter the path to the Private Key used to encrypt the files: ")
        passphrase = getpass("Enter the password used to store the key: ")
        decrypter = Decrypter(keyPath=keyPath, caesarShift=shift, passphrase=passphrase)
        noOfFiles = int(input("Enter the number of files you want to decrypt: "))
        while filesDone < noOfFiles:
            file = input("Enter the path of file to decrypt: ")
            output = input("Enter the path of output file. (Default = \"Decrypted_\"+Original File Name): ")
            decrypter.decryptFile(file, output)
            filesDone += 1
            print("Decrypted {} successfully. {} files remaining.".format(file, noOfFiles-filesDone))
    else:
        exit(0)
    print("Bye! See ya later :)\n")