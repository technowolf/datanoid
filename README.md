# Datanoid

### Multilevel Encryption Script to protect your Data.

Datanoid (release v2.0) is a CLI based Python Script which provides three-level encryption. 

**Level 1** - It converts the data to base64. It's the easiest way to convert anything to portable text. It's weakest level, but necessary for next level of encryptions.

**Level 2** - Now, applying Caesar's Cipher on the previous level, we do a little trick for data theives and your Non-Geek enemies. (boo-yah!) It isn't much strong, but only 1 in 25 attempt of Bruteforcing isn't a wise choice either. That too, after reaching here. See what comes next ;)

**Level 3** - THE FORTRESS FOR YOUR DATA. The Encryption on this level is true encryption, **_AES-128-CBC_**. The best part, key changes at each run, and a new encrypting cipher object is created for every file. Random IVs people. Bruteforcing it isn't superfast these days unless you're a superhuman or own a supercomputer. :')

##### GUI coming soon!
Changelog : 
v2.0 - Rewrote the whole program. More modular. Removed multi-threading as of now.

v1.1 - Multithreading Support for faster decryption/encryption
	   Using Python 3
	   More Secure PKCS1_OAEP Cipher

v1.0 - Initial Release
